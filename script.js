let lastScrollTop = 0;

window.addEventListener("scroll", function() {
  let scroll = window.pageYOffset || document.documentElement.scrollTop;
  let parallax = document.querySelector('.parallax');
  parallax.style.transform = 'translateY(' + (scroll * 0.4) + 'px)';
});

const API_URL = 'https://suitmedia-backend.suitdev.com/api/ideas';

const fetchIdeas = async (pageNumber = 1, pageSize = 10, sort = '-published_at') => {
  try {
    const response = await fetch(`${API_URL}?page[number]=${pageNumber}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sort}`);
    const data = await response.json();
    return data.data; // Data ide dari API
  } catch (error) {
    console.error('Error fetching ideas:', error);
    return [];
  }
};

const displayIdeas = async () => {
  const ideas = await fetchIdeas();

  if (ideas.length === 0) {
    console.log('No ideas found.');
    return;
  }

  const postsContainer = document.querySelector('.posts');

  ideas.forEach(idea => {
    const postElement = document.createElement('div');
    postElement.classList.add('post');

    const thumbnailUrl = idea.small_image;
    const imageUrl = idea.medium_image;
    const title = idea.title;

    const thumbnail = document.createElement('img');
    thumbnail.src = thumbnailUrl;
    thumbnail.loading = 'lazy';

    const image = document.createElement('img');
    image.src = imageUrl;
    image.loading = 'lazy';

    const titleElement = document.createElement('h2');
    titleElement.textContent = title;

    postElement.appendChild(thumbnail);
    postElement.appendChild(image);
    postElement.appendChild(titleElement);

    postsContainer.appendChild(postElement);
  });
};

displayIdeas(); // Memuat ide-ide saat halaman dimuat
  